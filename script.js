let myPokemon = {
	name: 'Bulbasaur',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function() {
		console.log('This pokemon tackled targetPokemon')
	},

	faint: function() {
		console.log('Pokemon fainted')
	}
}

console.log(myPokemon);


function Pokemon(name, level) {
	// properties
	this.name = name;
	this.level = level;
	this.health = 3 * level;
	this.attack = level;
	// methods
	this.tackle = function(target) {
		
		console.log(this.name + ' tackled '+ target.name);
		console.log(target.name + "'s health is now reduced to " + (target.health - this.attack));
		target.health -= this.attack
			if (target.health <= 5) {
				target.faint()
			}
		console.log()
	},
	this.faint = function() {
		console.log(this.name + " fainted.")
	}
}


let bulbasaur = new Pokemon("Bulbasaur", 16);

let charmander = new Pokemon("Charmander", 8);

bulbasaur.tackle(charmander)
bulbasaur.tackle(charmander)
